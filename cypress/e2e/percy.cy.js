const pages = ['http://example.com'];
const sizes = ['iphone-6', 'ipad-3', [1200, 800]];
describe('Visual Testing with Percy and Cypress', () => {

	it('should take percy snapshot', () => {
		cy.visit('http://example.com');
		cy.wait(1000);
		cy.percySnapshot('Homepage test');
	})
	
});
